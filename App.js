/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { SafeAreaView, I18nManager } from 'react-native';
import MainNavigator from './src/navigators/';
import { COLORS } from './src/constants';

type Props = {};
I18nManager.allowRTL(false);
export default class App extends Component<Props> {
  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.BLACK }}>
        <MainNavigator />
      </SafeAreaView>
    );
  }
}
