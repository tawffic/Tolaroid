export const STRINGS = {
  TABS: {
    NEARBY: 'NEARBY',
    UPCOMING: 'UPCOMING',
    PAST: 'PAST',
  },
  BUTTONS: {
    JOIN: 'JOIN',
    LOGIN: 'LOGIN',
    SPONSOR: 'Be our official sponsor/exhibitor',
    SHARE: 'Share',
    THANKS: 'Thanks',
    SAVE: 'Save',
    COPY: 'COPY',
    ATTEND: 'ATTEND',
    SAVE_ALBUM: 'SAVE ALBUM',
    SAVED: 'SAVED',
    LIKE: 'LIKE',
    COPY_AND_SHARE: 'COPY & SHARE',
  },
  EMPTY: {
    ALBUM: 'No Albums found for the selected area',
    SAVED_ALBUM: 'No Saved Albums',
    ALBUM_PHOTOS: 'The Album has no Photos',
  },
  SHARE: {
    ALBUM: 'Check this Album: ',
    PHOTO: 'Check this Photo: ',
  },

  NOTES: {},
  ERRORS: {},
};
