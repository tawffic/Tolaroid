import { StyleSheet } from 'react-native';
import { COLORS } from './../constants/';

export default StyleSheet.create({
  screenTitle: {
    height: 24,
    width: 37 * 3,
    marginLeft: 20,
    marginRight: 20,
  },
});
