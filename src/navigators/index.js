import React from 'react';
import { Image, Text } from 'react-native';
import { StackNavigator } from 'react-navigation';
import {
  Home,
  Album,
  Gallery,
  SavedAlbums,
  WebView,
  Test,
} from '../containers/';
import { COLORS, ICONS, IMAGES, FONTS } from './../constants/';
import { Icon, HeaderBtn } from './../components/';
import styles from './style';
const RouteConfigs = {
  Home: {
    screen: Home,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: <Image source={IMAGES.LOGO} style={styles.screenTitle} />,
        // headerRight: (
        //   <HeaderBtn onPress={() => {}}>
        //     <Icon source={ICONS.SEARCH} />
        //   </HeaderBtn>
        // ),
        // headerLeft: (
        //   <HeaderBtn onPress={() => {}}>
        //     <Icon source={ICONS.MENU} />
        //   </HeaderBtn>
        // ),
      };
    },
  },
  WebView: {
    screen: WebView,
    navigationOptions: ({ navigation }) => {
      return {
        // headerTitle: <Image source={IMAGES.LOGO} style={styles.screenTitle} />,
        headerTitle: (
          <Text style={FONTS.WHITE.HEADER_TITLE_BOLD}>Tolaroid.com</Text>
        ),

        headerLeft: (
          <HeaderBtn onPress={navigation.goBack}>
            <Icon source={ICONS.BACK} />
          </HeaderBtn>
        ),
      };
    },
  },
  Album: {
    screen: Album,
    navigationOptions: ({ navigation }) => {
      return {
        // headerRight: <Text>3 MB</Text>,
        headerTitle: <Image source={IMAGES.LOGO} style={styles.screenTitle} />,
        headerLeft: (
          <HeaderBtn onPress={navigation.goBack}>
            <Icon source={ICONS.BACK} />
          </HeaderBtn>
        ),
      };
    },
  },
  Gallery: {
    screen: Gallery,
    navigationOptions: ({ navigation }) => {
      return {
        header: null,
      };
    },
  },

  SavedAlbums: {
    screen: SavedAlbums,
    navigationOptions: ({ navigation }) => {
      return {
        headerTitle: (
          <Text style={FONTS.WHITE.HEADER_TITLE_BOLD}>Saved Albums</Text>
        ),
        headerLeft: (
          <HeaderBtn onPress={navigation.goBack}>
            <Icon source={ICONS.BACK} />
          </HeaderBtn>
        ),
      };
    },
  },
};
const StackNavigatorConfig = {
  //Sets the default screen of the stack. Must match one of the keys in route configs.
  initialRouteName: 'Home',
  // initialRouteName: 'WebView',
  // initialRouteName: 'Album',
  // initialRouteName: 'Gallery',

  //Specifies how the header should be rendered:
  // float - Render a single header that stays at the top and animates as screens are changed. This is a common pattern on iOS.
  // screen - Each screen has a header attached to it and the header fades in and out together with the screen. This is a common pattern on Android.
  // none - No header will be rendered.
  // headerMode: 'none',

  navigationOptions: {
    headerStyle: {
      backgroundColor: COLORS.BLACK,
      borderBottomWidth: 0,
    },
    gesturesEnabled: false,
  },
};

export default StackNavigator(RouteConfigs, StackNavigatorConfig);
