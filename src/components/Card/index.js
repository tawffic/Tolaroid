'use strict';
import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import { styles } from './style';
import { Icon } from './../';
import { FONTS } from './../../constants';
import { openWebView } from '../../services';

export default class Card extends React.PureComponent {
  cardClick = () => {
    const { card, navigate } = this.props;
    const { screen, openURL } = card;
    screen ? navigate(screen) : openWebView(navigate, openURL);
  };
  render() {
    const { card, navigate } = this.props;
    const { title, subTitle, icon, openURL } = card;
    if (!openURL) return null;
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.btnContainer} onPress={this.cardClick}>
          <Icon source={icon} iconStyle={styles.cardIcon} />
          <Text style={FONTS.BLACK.MAIN_BOLD} numberOfLines={1}>
            {title}
          </Text>
          <Text style={FONTS.GRAY.MAIN_BOLD} numberOfLines={1}>
            {subTitle}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
