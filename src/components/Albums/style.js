import { StyleSheet } from 'react-native';
import { COLORS } from './../../constants/';

export default StyleSheet.create({
  sliderContentContainer: {
    paddingVertical: 10, // for custom animation
  },
});
