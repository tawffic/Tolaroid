import React, { Component } from 'react';
import { View } from 'react-native';
import Carousel, { getInputRangeFromIndexes } from 'react-native-snap-carousel';
import { AlbumItem, Empty, ActivityIndicator } from './../';
import styles from './style';
import { sliderWidth, itemWidth } from './../AlbumItem/style';
import { STRINGS } from '../../constants';

export default class Albums extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     loaded: false,
  //   };
  //   setTimeout(() => this.setState({ loaded: true }), 2000);
  // }
  stackScrollInterpolator = (index, carouselProps) => {
    const range = [1, 0, -1, -2, -3];
    const inputRange = getInputRangeFromIndexes(range, index, carouselProps);
    const outputRange = range;
    return { inputRange, outputRange };
  };

  stackAnimatedStyles = (index, animatedValue, carouselProps) => {
    const sizeRef = carouselProps.vertical
      ? carouselProps.itemHeight
      : carouselProps.itemWidth;
    const translateProp = carouselProps.vertical ? 'translateY' : 'translateX';

    const cardOffset = 18;
    const card1Scale = 0.9;
    const card2Scale = 0.8;

    const getTranslateFromScale = (index, scale) => {
      const centerFactor = 1 / scale * index;
      const centeredPosition = -Math.round(sizeRef * centerFactor);
      const edgeAlignment = Math.round((sizeRef - sizeRef * scale) / 2);
      const offset = Math.round(cardOffset * Math.abs(index) / scale);

      return centeredPosition - edgeAlignment - offset;
    };

    return {
      opacity: animatedValue.interpolate({
        inputRange: [-3, -2, -1, 0],
        outputRange: [0, 0.5, 0.75, 1],
        extrapolate: 'clamp',
      }),
      transform: [
        {
          scale: animatedValue.interpolate({
            inputRange: [-2, -1, 0, 1],
            outputRange: [card2Scale, card1Scale, 1, card1Scale],
            extrapolate: 'clamp',
          }),
        },
        {
          [translateProp]: animatedValue.interpolate({
            inputRange: [-3, -2, -1, 0, 1],
            outputRange: [
              getTranslateFromScale(-3, card2Scale),
              getTranslateFromScale(-2, card2Scale),
              getTranslateFromScale(-1, card1Scale),
              0,
              sizeRef * 0.5,
            ],
            extrapolate: 'clamp',
          }),
        },
      ],
    };
  };

  render() {
    const {
      data,
      openAlbum,
      openProfile,
      emptyMessage = STRINGS.EMPTY.ALBUM,
      loaded,
    } = this.props;

    if (!loaded) return <ActivityIndicator loaded={loaded} />;
    else if (data.length == 0) return <Empty message={emptyMessage} />;
    return (
      <Carousel
        data={data}
        renderItem={({ item }) => (
          <AlbumItem
            erEntry
            data={item}
            openAlbum={openAlbum}
            openProfile={openProfile}
          />
        )}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        // containerCustomStyle={styles.slider}
        contentContainerCustomStyle={styles.sliderContentContainer}
        layout={'stack'}
        scrollInterpolator={this.stackScrollInterpolator}
        slideInterpolatedStyle={this.stackAnimatedStyles}
        useScrollView={true} // <--- Use this for a better effect or disable it to get performance optimizations
      />
    );
  }
}
