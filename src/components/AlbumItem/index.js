import React, { Component } from 'react';
import moment from 'moment';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import { UserRow } from './../';
import styles from './style';
import { IMAGES } from '../../constants';
import { openExternalLink } from '../../services/';

export default class AlbumItem extends Component {
  render() {
    const { data, openAlbum } = this.props;
    const {
      album,
      albumname,
      start,
      user,
      location,
      locationLat,
      locationLng,
    } = data;
    const { image } = album;
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={styles.slideInnerContainer}
        onPress={() => openAlbum(data)}
      >
        <View style={styles.imageContainer}>
          {/* <View style={styles.image}> */}
          {image ? (
            <ImageBackground source={{ uri: image }} style={styles.image}>
              <UserRow owner={user} />
            </ImageBackground>
          ) : (
            <Image
              style={styles.image}
              source={{
                uri: IMAGES.ALBUM_IMAGE_FALLBACK,
              }}
            />
          )}
          {/* </View> */}
          <View style={styles.radiusMask} />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title} numberOfLines={1}>
            {albumname}
          </Text>
          <Text style={styles.subtitle} numberOfLines={1}>
            {moment(start).format('dddd, DD MMMM YYYY, h:mm A')}
          </Text>
          <TouchableOpacity
            onPress={() =>
              openExternalLink(
                `https://www.google.com/maps/?q=${locationLat},${locationLng}`,
              )
            }
          >
            <Text style={styles.subtitle} numberOfLines={1}>
              {location}
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
}
