import { Platform } from 'react-native';
import LocationSwitch from 'react-native-location-switch';

const getLocation = cb => {
  console.log('Fetch Location');
  navigator.geolocation.getCurrentPosition(
    position => {
      // console.log('position', position);
      cb(position);
    },
    error => {
      cb(false);
      // console.log('error', error);
    },
    {
      enableHighAccuracy: false,
      // enableHighAccuracy: true,
      timeout: 10000,
      // maximumAge: 3600000,
    },
  );
};

const onEnableLocationPress = cb => {
  LocationSwitch.enableLocationService(
    1000,
    true,
    () => {
      getLocation(cb);
    },
    () => {
      cb(false);
      // console.log('Disable Alert of location!!!');
    },
  );
};

export const checkAvailabilityOFLocation = cb => {
  if (Platform.OS == 'android') {
    LocationSwitch.isLocationEnabled(
      () => {
        getLocation(cb);
      },
      () => {
        onEnableLocationPress(cb);
      },
    );
  } else {
    getLocation(cb);
  }
};
