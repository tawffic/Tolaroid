import React, { Component } from 'react';
import { View, FlatList, Image } from 'react-native';
import ScrollableTabView, {
  DefaultTabBar,
} from 'react-native-scrollable-tab-view';
import { Albums, CustomBtn, Card } from './../../components/';
import {
  checkAvailabilityOFLocation,
  requestAlbums,
  openWebView,
} from './../../services/';
import {
  COLORS,
  CARDS,
  STRINGS,
  WEB_URL_SIGNIN,
  WEB_URL_JOIN,
  IS_ANDROID,
} from './../../constants/';
import styles from './style';

type Props = {};

export default class Home extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      nearByAlbums: [],
      upcomingAlbums: [],
      pastAlbums: [],
      loaded: false,
    };
  }

  componentDidMount() {
    checkAvailabilityOFLocation(this.getLocation);
  }

  getLocation = async location => {
    // console.log('location', location);
    if (location) {
      const { coords } = location;
      const nearByData = await requestAlbums(coords, true);
      const upcomingData = await requestAlbums(coords, false, true);
      const pastData = await requestAlbums(coords, false, false);
      const nearByAlbums = nearByData.data.schedules;
      const upcomingAlbums = upcomingData.data.schedules;
      const pastAlbums = pastData.data.schedules;
      this.setState({
        nearByAlbums,
        upcomingAlbums,
        pastAlbums,
        loaded: true,
      });
    }
  };

  /**
   * open Album screen
   */
  openAlbum = album => {
    const { navigation } = this.props;
    navigation.navigate('Album', { album });
  };

  /**
   * open user Profile
   */
  openProfile = () => {};

  /**
   * open Login
   */
  openLogin = () => {
    const { navigation } = this.props;
    openWebView(navigation.navigate, WEB_URL_SIGNIN);
  };

  /**
   * open Join
   */
  openJoin = () => {
    const { navigation } = this.props;
    openWebView(navigation.navigate, WEB_URL_JOIN);
  };

  render() {
    const { nearByAlbums, upcomingAlbums, pastAlbums, loaded } = this.state;
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <ScrollableTabView
            tabBarUnderlineStyle={styles.tabUnderLine}
            tabBarBackgroundColor={COLORS.BLACK}
            tabBarActiveTextColor={COLORS.BLACK}
            tabBarInactiveTextColor={COLORS.WHITE}
            tabBarActiveStyle={styles.activeTab}
            tabBarTextStyle={styles.tabText}
            style={styles.scrollingTabs}
            initialPage={0}
            renderTabBar={() => <DefaultTabBar style={styles.tabBar} />}
          >
            <View tabLabel={STRINGS.TABS.NEARBY}>
              <Albums
                data={nearByAlbums}
                openAlbum={this.openAlbum}
                openProfile={this.openProfile}
                loaded={loaded}
              />
            </View>

            <View tabLabel={STRINGS.TABS.UPCOMING}>
              <Albums
                data={upcomingAlbums}
                openAlbum={this.openAlbum}
                openProfile={this.openProfile}
                loaded={loaded}
              />
            </View>

            <View tabLabel={STRINGS.TABS.PAST}>
              <Albums
                data={pastAlbums}
                openAlbum={this.openAlbum}
                openProfile={this.openProfile}
                loaded={loaded}
              />
            </View>
          </ScrollableTabView>
          <View style={styles.cardsContainer}>
            <FlatList
              data={CARDS}
              horizontal={true}
              renderItem={({ item }) => (
                <Card card={item} navigate={navigation.navigate} />
              )}
              keyExtractor={(item, index) => index.toString()}
              showsHorizontalScrollIndicator={false}
            />
          </View>

          <View style={[styles.BtnsContainer, { opacity: IS_ANDROID ? 1 : 0 }]}>
            <CustomBtn
              btnText={STRINGS.BUTTONS.JOIN}
              onPress={this.openJoin}
              isLight={false}
            />
            <CustomBtn
              btnText={STRINGS.BUTTONS.LOGIN}
              onPress={this.openLogin}
              isLight={true}
            />
          </View>
        </View>
      </View>
    );
  }
}
