import { StyleSheet } from 'react-native';
import { COLORS } from './../../constants/';

export default StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: COLORS.WHITE,
    backgroundColor: COLORS.BLACK,
  },
  activeTab: {
    backgroundColor: COLORS.WHITE,
    borderRadius: 20,
    margin: 8,
  },
  tabText: { fontSize: 15 },
  tabUnderLine: { backgroundColor: COLORS.BLACK },
  tabBar: { borderWidth: 0 },
  scrollingTabs: {
    marginTop: 10,
    maxHeight: 450,
    // backgroundColor: 'red',
  },
  slider: {
    marginTop: 15,
    overflow: 'visible', // for custom animations
  },
  cardsContainer: {
    marginLeft: 5 * 3,
    marginTop: 4 * 3,
  },
  BtnsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 20,
    marginLeft: 20,
    marginTop: 5 * 3,
    marginBottom: 5 * 3,
  },
});
