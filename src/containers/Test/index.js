import React, { Component } from 'react';
import { View, FlatList, Image } from 'react-native';
import styles from './style';

type Props = {};

export default class Text extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Image
            source={require('./../../assets/1.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/2.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/3.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/4.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/5.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/6.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/7.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/8.jpg')}
            style={{ height: 0 }}
          />
          <Image
            source={require('./../../assets/9.jpg')}
            style={{ height: 0 }}
          />
        </View>
      </View>
    );
  }
}
