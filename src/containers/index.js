import Home from './Home/';
import Album from './Album/';
import Gallery from './Gallery/';
import SavedAlbums from './SavedAlbums/';
import WebView from './WebView/';
import Test from './Test/';

export { Home, Album, Gallery, SavedAlbums, WebView, Test };
