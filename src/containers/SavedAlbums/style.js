import { StyleSheet } from 'react-native';
import { COLORS } from './../../constants/';

export default StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // backgroundColor: COLORS.WHITE,
    backgroundColor: COLORS.BLACK,
  },
  albumPhotosCarousel: {
    marginTop: 5,
  },
  slide: {
    marginLeft: 5,
    marginRight: 5,
  },
  savePhotos: {
    alignItems: 'center',
    marginTop: -18,
    marginBottom: 10,
  },
});
