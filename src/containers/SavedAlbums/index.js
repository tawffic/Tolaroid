import React, { Component } from 'react';
import { View, Text, Dimensions, ScrollView } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { Albums, AlbumPhotoItem, Empty } from './../../components/';
import { getSavedAlbumsData, getSavedPhotosData } from './../../services/';
import { STRINGS, FONTS } from './../../constants/';
import { sliderWidth } from './../../components/AlbumItem/style';
import styles from './style';
const { width, height } = Dimensions.get('window');
export default class SavedAlbums extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      savedAlbums: [],
      savedPhotos: [],
    };
  }

  async componentDidMount() {
    let savedAlbums = await getSavedAlbumsData();
    let savedPhotos = await getSavedPhotosData();
    savedAlbums = savedAlbums ? savedAlbums : [];
    savedPhotos = savedPhotos ? savedPhotos : [];
    const currentPhoto = savedPhotos.length > 0 ? savedPhotos[0] : {};
    this.setState({ savedAlbums, savedPhotos, currentPhoto });
  }

  openAlbum = album => {
    const { navigation } = this.props;
    navigation.navigate('Album', { album });
  };

  openProfile = () => {};

  openPhoto = startingIndex => {
    const { navigation } = this.props;
    const { savedPhotos } = this.state;
    navigation.navigate('Gallery', { photos: savedPhotos, startingIndex });
  };

  changeCurrentItem = index => {
    const { savedPhotos } = this.state;
    if (savedPhotos.length && index > 0) {
      const currentPhoto = savedPhotos[index];
      this.setState({ currentPhoto });
    }
  };
  render() {
    const { savedAlbums, savedPhotos, currentPhoto } = this.state;
    return (
      <View style={styles.container}>
        <ScrollView>
          {/* saved albums */}
          <Albums
            data={savedAlbums}
            openAlbum={this.openAlbum}
            openProfile={this.openProfile}
            emptyMessage={STRINGS.EMPTY.SAVED_ALBUM}
            loaded
          />
          {/* saved photos */}
          <View style={styles.savePhotos}>
            <Text style={FONTS.WHITE.HEADER_TITLE_BOLD}>Saved Photos</Text>
          </View>
          {savedPhotos.length ? (
            <View style={styles.topPart}>
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={savedPhotos ? savedPhotos : []}
                renderItem={({ item, index }) => (
                  <AlbumPhotoItem
                    erEntry
                    index={index}
                    data={item}
                    openProfile={this.openProfile}
                    openPhoto={this.openPhoto}
                    allowShare={false}
                  />
                )}
                sliderWidth={sliderWidth}
                itemWidth={width - 50}
                layout={'default'}
                contentContainerCustomStyle={styles.albumPhotosCarousel}
                inactiveSlideOpacity={1}
                inactiveSlideScale={1}
                slideStyle={styles.slide}
                onEndReachedThreshold={0.2}
                keyExtractor={(item, index) => index.toString()}
                onSnapToItem={this.changeCurrentItem}
              />
            </View>
          ) : (
            <Empty message={STRINGS.EMPTY.ALBUM_PHOTOS} />
          )}
        </ScrollView>
      </View>
    );
  }
}
