import React, { Component } from 'react';
import { View } from 'react-native';
import GalleryPhotos from 'react-native-image-gallery';
import _ from 'lodash';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import styles from './style';
import { Icon, HeaderBtn, ShareModal, IconBtn } from './../../components/';
import { ICONS, COLORS, STRINGS } from '../../constants';
import {
  getSavedPhotosData,
  savePhoto,
  isPhotoSaved,
  removeSavedPhotosData,
} from './../../services';

export default class Gallery extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = { isShareModalOpened: false };
  }

  async componentWillMount() {
    const { navigation } = this.props;
    const { params } = navigation.state;
    const { photos, startingIndex, albumDetails } = params;
    //library vonstarains should take 'uri' prop to show it
    const newPhotos = _.map(photos, function(item) {
      item = _.assign({ uri: item.photoUrl }, item);
      return { source: item };
    });
    this.setState({ startingIndex, newPhotos, photos, albumDetails });

    // removeSavedAlbumsData();
    // const savedPhotos = await getSavedPhotosData();
    // console.log('saved Photos ', savedPhotos);
  }

  render() {
    const { navigation } = this.props;
    const {
      newPhotos,
      startingIndex,
      photos,
      albumDetails,
      isShareModalOpened,
    } = this.state;
    const photoObject = photos[startingIndex];
    const { photoUrl } = photoObject;
    // console.log('#startingIndex ', newPhotos);
    return (
      <View style={styles.container}>
        <View style={styles.closeContainer}>
          <HeaderBtn onPress={navigation.goBack}>
            <Icon source={ICONS.CLOSE} />
          </HeaderBtn>
        </View>
        <GalleryPhotos
          style={styles.image}
          initialPage={startingIndex}
          images={newPhotos}
          // images={images}
          pageMargin={10}
          onPageSelected={index => this.setState({ startingIndex: index })}
          imageComponent={props => (
            <Image
              {...props}
              indicator={Progress.Circle}
              indicatorProps={{
                size: 20,
                color: COLORS.ORANGE,
                unfilledColor: COLORS.BLACK,
              }}
            />
          )}
        />
        {albumDetails ? (
          <View style={styles.shareBtnContainer}>
            <IconBtn
              onPress={() => this.setState({ isShareModalOpened: true })}
              icon={ICONS.FAVORITE}
              text={STRINGS.BUTTONS.THANKS}
            />
            <IconBtn
              onPress={() => savePhoto(photoObject)}
              icon={ICONS.SAVE_PHOTOS}
              text={STRINGS.BUTTONS.SAVE}
            />
            <IconBtn
              onPress={() => this.setState({ isShareModalOpened: true })}
              icon={ICONS.SHARE}
              text={STRINGS.BUTTONS.SHARE}
            />
          </View>
        ) : null}
        {albumDetails ? (
          <ShareModal
            onClosed={() => {
              this.setState({ isShareModalOpened: false });
            }}
            isOpened={isShareModalOpened}
            albumDetails={albumDetails}
            imageUrl={photoUrl}
          />
        ) : null}
      </View>
    );
  }
}
