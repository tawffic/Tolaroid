import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Dimensions,
  InteractionManager,
  FlatList,
} from 'react-native';
import _ from 'lodash';
import Carousel from 'react-native-snap-carousel';
import {
  Icon,
  AlbumPhotoItem,
  Ads,
  ActivityIndicator,
  Empty,
  HeaderBtn,
  NewPhotosNotify,
  Switch,
  ShareModal,
  CustomBtn,
  Card,
} from './../../components/';
import { ICONS, FONTS, STRINGS } from './../../constants/';
import { LOAD_MORE_PHOTOS } from './../../config//';
import styles from './style';
import { sliderWidth } from './../../components/AlbumItem/style';
import {
  requestAbumPhotos,
  requestAlbumDetails,
  requestOffers,
  getSavedAlbumsData,
  saveAlbum,
  removeSavedAlbumsData,
  isAlbumSaved,
  getAlbumUrl,
  openExternalLink,
  initializeAlbumCardsWithLinks,
} from './../../services';
const { width, height } = Dimensions.get('window');

export default class Album extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    if (params) {
      const { album, showShareModal } = params;
      const { albumname, user } = album;
      const { username } = user;
      return {
        headerRight:
          username && albumname ? (
            <HeaderBtn onPress={() => showShareModal(true)}>
              <Icon source={ICONS.SHARE} />
            </HeaderBtn>
          ) : null,
      };
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      currentPhoto: {},
      offers: [],
      album: {},
      loaded: false,
      isAutoReloadMode: false,
      isStopSLideShow: false,
      isHideButtonsMode: false,
      isShareModalOpened: false,
      shareAlbum: false,
    };
  }

  async componentDidMount() {
    const { navigation } = this.props;
    const { params } = navigation.state;
    const { album } = params;
    const { albumId, albumname, user } = album;
    const { id } = user;
    const photosData = await requestAbumPhotos(albumId);
    const albumData = await requestAlbumDetails(albumname, id);
    const offersData = await requestOffers(albumId);
    const photos = photosData.data.photos;
    const currentPhoto = photos.length > 0 ? photos[0] : {};
    const albumDetails = albumData.data.albums[0];
    const meta = photosData.data.meta;
    const offers = offersData.data.sponsorCampaigns;
    const isSaved = await isAlbumSaved(album);
    const eventCards = albumDetails
      ? initializeAlbumCardsWithLinks(albumDetails)
      : null;
    this.setState({
      photos,
      currentPhoto,
      meta,
      offers,
      album,
      albumId,
      albumDetails,
      loaded: true,
      isSaved,
      eventCards,
    });

    this._interval = setInterval(this.pollingRequestPhotos, 2000);
    this.autoplayOffersCarousel();

    InteractionManager.runAfterInteractions(() => {
      navigation.setParams({
        showShareModal: this.showShareModal,
      });
    });

    // removeSavedAlbumsData();
    // const albums = await getSavedAlbumsData();
    // console.log('saved albums ', albums);
  }

  componentWillUnmount() {
    clearInterval(this._interval);
    this.stopAutoplayOffersCarousel();
  }

  pollingRequestPhotos = async () => {
    const { photos, isAutoReloadMode, albumId } = this.state;
    const photosData = await requestAbumPhotos(albumId);
    const newPhotos = photosData.data.photos;
    const meta = photosData.data.meta;
    if (newPhotos.length) {
      //check if the first photo id changed
      const firstNewID = newPhotos[0].id;
      const firstID = photos[0].id;
      if (firstNewID !== firstID) {
        // console.log('there are changed happend');
        //make file changed is one temporary
        newAddedPhotos = 1;
        if (isAutoReloadMode) {
          this.setState({ photos: newPhotos, meta });
        } else {
          this.setState({
            newAddedPhotos,
            newPhotos,
            meta,
          });
        }
      }
    }
  };

  changeCurrentItem = index => {
    const { photos } = this.state;
    if (photos.length && index > 0) {
      const currentPhoto = photos[index];
      this.setState({ currentPhoto });
    }
  };

  openProfile = () => {};

  openPhoto = startingIndex => {
    const { navigation } = this.props;
    const { photos, albumDetails } = this.state;
    navigation.navigate('Gallery', { photos, startingIndex, albumDetails });
  };

  newPhotosClicked = () => {
    const { newPhotos } = this.state;
    //update photos
    this.setState({ photos: newPhotos, newAddedPhotos: 0 });
    //restart album
    this._carousel.snapToItem(0);
  };

  loadMoreData = async () => {
    const { photos, albumId, meta } = this.state;
    const { nextPage } = meta;
    if (nextPage) {
      const lastPhotoId = photos[photos.length - 1].id;
      const photosData = await requestAbumPhotos(
        albumId,
        lastPhotoId,
        LOAD_MORE_PHOTOS,
      );

      const newPhotos = photosData.data.photos;
      const meta = photosData.data.meta;
      this.setState({
        photos: _.uniqBy([...photos, ...newPhotos], 'id'),
        meta,
      });
    }
  };

  onPressAttend = () => {
    const { albumDetails } = this.state;
    const { username, name } = albumDetails;
    if (username && name) {
      const albumURL = getAlbumUrl(username, name);
      openExternalLink(albumURL);
    }
  };

  onPressSaveAlbum = () => {
    const { album } = this.state;
    saveAlbum(album);
    this.setState({ isSaved: true });
  };

  onPressLike = () => {
    const { albumDetails } = this.state;
    const { eventLinkLike } = albumDetails;
    if (eventLinkLike) {
      openExternalLink(eventLinkLike);
    }
  };

  toggleSlideShow = value => {
    this.setState({ isStopSLideShow: value });
    // this._offersCarousel.snapToItem(0);
    value ? this.stopAutoplayOffersCarousel() : this.autoplayOffersCarousel();
  };

  toggleMode = value => {
    this.setState({ isAutoReloadMode: value });
  };

  toggleHideButtonMode = value => {
    this.setState({ isHideButtonsMode: value });
  };

  showShareModal = (shareAlbum = false) => {
    this.setState({ isShareModalOpened: true, shareAlbum });
  };

  autoplayOffersCarousel() {
    const { offers } = this.state;
    if (offers.length && this._offersCarousel) {
      this._offersCarouselInterval = setInterval(
        () => this._offersCarousel.snapToNext(),
        2000,
      );
      // this._offersCarousel.startAutoplay();
    }
  }

  stopAutoplayOffersCarousel() {
    const { offers } = this.state;
    if (offers.length) {
      clearInterval(this._offersCarouselInterval);
      // this._offersCarousel.stopAutoplay();
    }
  }

  render() {
    const {
      photos,
      currentPhoto,
      loaded,
      offers,
      newAddedPhotos,
      album,
      albumDetails,
      isStopSLideShow,
      isAutoReloadMode,
      isHideButtonsMode,
      isShareModalOpened,
      shareAlbum,
      isSaved,
      eventCards,
    } = this.state;

    const { navigation } = this.props;
    const { user } = album;
    const tagList = albumDetails ? albumDetails.tagList : null;
    // (!loaded) return <ActivityIndicator loaded={loaded} />;
    return (
      <View style={styles.container}>
        {!loaded ? (
          <ActivityIndicator loaded={loaded} />
        ) : (
          <View style={styles.container}>
            <ScrollView>
              <View style={styles.headerBtnsContainer}>
                <CustomBtn
                  btnText={STRINGS.BUTTONS.ATTEND}
                  style={styles.actionBTN}
                  textStyle={styles.textStyle}
                  onPress={this.onPressAttend}
                  isLight={false}
                />
                <CustomBtn
                  btnText={
                    isSaved ? STRINGS.BUTTONS.SAVED : STRINGS.BUTTONS.SAVE_ALBUM
                  }
                  style={styles.actionBTN}
                  textStyle={styles.textStyle}
                  onPress={this.onPressSaveAlbum}
                  isLight={true}
                  icon={isSaved ? ICONS.RIGHT : null}
                />
                <CustomBtn
                  btnText={STRINGS.BUTTONS.LIKE}
                  style={styles.actionBTN}
                  textStyle={styles.textStyle}
                  onPress={this.onPressLike}
                  isLight={false}
                />
              </View>
              {/* HashTags */}
              {isHideButtonsMode ? (
                <View style={styles.tagsContainer}>
                  <ScrollView horizontal>
                    {tagList.map((hashTag, key) => (
                      <Text key={key} style={FONTS.WHITE.HEADER_TITLE_BOLD}>
                        #{hashTag}{' '}
                      </Text>
                    ))}
                  </ScrollView>
                </View>
              ) : null}
              {/* Albums Photos Carousel */}
              {photos.length ? (
                <View style={styles.topPart}>
                  <Carousel
                    ref={c => {
                      this._carousel = c;
                    }}
                    data={photos ? photos : []}
                    renderItem={({ item, index }) => (
                      <AlbumPhotoItem
                        erEntry
                        index={index}
                        data={item}
                        owner={user}
                        openProfile={this.openProfile}
                        openPhoto={this.openPhoto}
                        clickOnShare={this.showShareModal}
                      />
                    )}
                    sliderWidth={sliderWidth}
                    itemWidth={width - 50}
                    layout={'default'}
                    contentContainerCustomStyle={styles.albumPhotosCarousel}
                    inactiveSlideOpacity={1}
                    inactiveSlideScale={1}
                    slideStyle={styles.slide}
                    // layoutCardOffset={0}
                    // activeSlideOffset={0}
                    // useScrollView={true}
                    onEndReached={this.loadMoreData}
                    onEndReachedThreshold={0.2}
                    keyExtractor={(item, index) => index.toString()}
                    onSnapToItem={this.changeCurrentItem}
                  />
                  {newAddedPhotos ? (
                    <NewPhotosNotify
                      newAddedPhotos={newAddedPhotos}
                      onPress={this.newPhotosClicked}
                    />
                  ) : null}
                </View>
              ) : (
                <Empty message={STRINGS.EMPTY.ALBUM_PHOTOS} />
              )}
              {/* Sponsor BTN */}
              {!isHideButtonsMode ? (
                <View style={styles.cardsContainer}>
                  <FlatList
                    data={eventCards}
                    horizontal={true}
                    renderItem={({ item }) => (
                      <Card card={item} navigate={navigation.navigate} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    showsHorizontalScrollIndicator={false}
                  />
                </View>
              ) : null}
              {/* OFFERS Carousel */}
              <Carousel
                ref={c => {
                  this._offersCarousel = c;
                }}
                data={offers ? offers : []}
                renderItem={({ item }) => (
                  <Ads offer={item} isHideButtonsMode={isHideButtonsMode} />
                )}
                sliderWidth={sliderWidth}
                itemWidth={width - 50}
                layout={'default'}
                contentContainerCustomStyle={styles.adsCarousel}
                loop
                // autoplay
                // autoplayDelay={2000}
                // autoplayInterval={3000}
              />
              {/* SWITCHES */}
              {offers.length > 0 ? (
                <Switch
                  onPress={this.toggleSlideShow}
                  text="Stop Slideshow"
                  isOn={isStopSLideShow}
                />
              ) : null}
              <Switch
                onPress={this.toggleMode}
                text="Auto Reload Photos"
                isOn={isAutoReloadMode}
              />
              <Switch
                onPress={this.toggleHideButtonMode}
                text="Hide Buttons"
                isOn={isHideButtonsMode}
              />
            </ScrollView>
            <ShareModal
              onClosed={() => {
                this.setState({ isShareModalOpened: false });
              }}
              isOpened={isShareModalOpened}
              albumDetails={albumDetails}
              imageUrl={
                currentPhoto && !shareAlbum ? currentPhoto.photoUrl : null
              }
            />
          </View>
        )}
      </View>
    );
  }
}
